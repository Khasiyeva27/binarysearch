public class Binary {
    public int binarySearch(int arr[], int n) {
        int start = 0;
        int end = arr.length - 1;

        while (start <= end) {
            int mid = start + (end - start) / 2;

            if (n < arr[mid]) {
                end = mid - 1;
            } else if (n > arr[mid]) {
                start = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
